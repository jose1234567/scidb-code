Configure like this to build on Freebsd 12.2

```
SYS_LDFLAGS="-L/usr/local/lib -lexecinfo -lcompat" \
SYS_CFLAGS="-I/usr/local/include" \
SYS_CXXFLAGS="-D__BSD__ -D_LIBCPP_STDLIB_INCLUDE_NEXT -I/usr/local/include \
-I/usr/src/contrib/ofed/include -I/usr/src/sys/cddl/contrib/opensolaris/uts/common" \
./configure --gcc-version=clang
```